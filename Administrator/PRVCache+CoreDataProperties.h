//
//  PRVCache+CoreDataProperties.h
//  
//
//  Created by Ruslan on 7/11/17.
//
//

#import "PRVCache+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface PRVCache (CoreDataProperties)

+ (NSFetchRequest<PRVCache *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSString *json;
@property (nullable, nonatomic, copy) NSString *key;
@property (nullable, nonatomic, copy) NSString *params;
@property (nullable, nonatomic, copy) NSNumber *seconds;

@end

NS_ASSUME_NONNULL_END
