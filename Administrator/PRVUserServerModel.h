//
//  PRVUserServerModel.h
//  FitnessApp
//
//  Created by Ruslan on 7/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVInfoServerModel;
@class PRVTicketSeverModel;
@class PRVCoachServerModel;
@class PRVProgramServerModel;
@class PRVClubServerModel;

typedef NS_ENUM(NSUInteger, PRVEnrollTicketType) {
    PRVEnrollTicketTypeFreeSchedule,
    PRVEnrollTicketTypePersonalCoach,
    PRVEnrollTicketTypeDefault,
};

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface PRVUserServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) PRVInfoServerModel *info;

+ (EKObjectMapping *)objectMapping;

- (PRVTicketSeverModel *)ticketForCode:(NSString *)code;

- (void)updateTicket:(PRVTicketSeverModel *)ticket;

- (NSArray <PRVTicketSeverModel *> *)ticketsForEnter;

- (NSArray <PRVTicketSeverModel *> *)ticketsForExit;

@end


@interface PRVInfoServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *accessToken;

@property (nonatomic, copy) NSString *email;

@property (nonatomic, assign) NSInteger _id;

@property (nonatomic, assign) NSInteger balance;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *image2;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) BOOL verify;

@property (nonatomic, strong) NSArray <PRVTicketSeverModel *> *tickets;

@property (nonatomic, strong) PRVClubServerModel *club;

+ (EKObjectMapping *)objectMapping;

- (NSArray <PRVTicketSeverModel *> *)tickets;

@end


@interface PRVTicketSeverModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *date;

@property (nonatomic, copy) NSString *dateActive;

@property (nonatomic, copy) NSString *dateComplete;

@property (nonatomic, copy) NSString *dateCancel;

@property (nonatomic, copy) NSString *datePayment;

@property (nonatomic, copy) NSString *code;

@property (nonatomic, assign) NSInteger failMinutes;

@property (nonatomic, assign) NSInteger failPrice;

@property (nonatomic, assign) NSInteger _id;

@property (nonatomic, assign) BOOL free;

@property (nonatomic, assign) BOOL isPayment;

@property (nonatomic, strong) PRVProgramServerModel *program;

@property (nonatomic, assign) NSInteger price;

@property (nonatomic, copy) NSString *timeActive;

@property (nonatomic, copy) NSString *timeCancel;

@property (nonatomic, copy) NSString *timeComplete;

@property (nonatomic, copy) NSString *timePayment;

@property (nonatomic, assign) NSInteger totalPrice;

@property (nonatomic, copy) NSString *time_from;

@property (nonatomic, copy) NSString *time_to;

@property (nonatomic, assign) NSInteger wallet;

@property (nonatomic, assign) NSInteger workMinutes;

@property (nonatomic, strong) PRVClubServerModel *club;

@property (nonatomic, strong) PRVCoachServerModel *coach;

@property (nonatomic, strong) PRVUserServerModel *user;

+ (EKObjectMapping *)objectMapping;

- (BOOL)isActive;

- (BOOL)isNew;

- (BOOL)isCompleted;

- (BOOL)isCanceled;

- (PRVEnrollTicketType)enrollmentType;

- (NSInteger)trainingDurationInSeconds;

- (NSInteger)secondsToFinishTraining;

- (NSInteger)minutesToFinishTraining;

- (NSInteger)minutesToStartTraining;

- (NSString *)dateToFinishTraining;

- (BOOL)isActive;

- (BOOL)isNew;

- (BOOL)isCompleted;

- (BOOL)isCanceled;

- (BOOL)isStarted;

- (BOOL)isEnded;

- (BOOL)isFailTimeStarted;

- (NSInteger)currentFailMinutes;

- (NSInteger)currentMinutesToExit;

- (NSInteger)totalSecondsToExit;

- (NSInteger)currentSecondsToExit;

@end

