//
//  PRVTicketActionAlertViewController.h
//  Administrator
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVTicketActionAlertViewController : UIViewController

@property (nonatomic, copy) void (^block)(BOOL scan);

@end
