//
//  PRVServerManager.h
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVClubsServerModel;
@class PRVUserServerModel;
@class PRVClubServerModel;
@class PRVTicketSeverModel;

#import <Foundation/Foundation.h>


@interface PRVServerManager : NSObject

+ (instancetype)sharedManager;

- (void)loadOptionsOnSuccess:(void(^)()) success
                   onFailure:(void(^)(NSError *error)) failure;

- (void)logInWithParams:(NSDictionary *)params
              onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)restorePasswordWithEmail:(NSString *)email
                       onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
                       onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)payTicketWithCode:(NSString *)code
              onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)updateTicketWithCode:(NSString *)code
                       enter:(BOOL)enter
                onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
                onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)verifyUserWithID:(NSInteger)userId
              onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
              onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;

- (void)getTicketListOnSuccess:(void(^)(NSArray <PRVTicketSeverModel *> *tickets, NSString *error)) success
                     onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure;
@end
