//
//  NSString+PRVExtension.h
//  FitnessApp
//
//  Created by Ruslan on 7/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (PRVExtension)

- (NSDictionary *)prv_dictionaryValue;

@end
