//
//  NSDictionary+PRVExtension.m
//  FitnessApp
//
//  Created by Ruslan on 7/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "NSDictionary+PRVExtension.h"

@implementation NSDictionary (PRVExtension)

- (NSString *)stringValue{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:0 error:&error];
    NSString *string =  [[NSString alloc] initWithData:jsonData
                                              encoding:NSUTF8StringEncoding];
    return string;
}

@end
