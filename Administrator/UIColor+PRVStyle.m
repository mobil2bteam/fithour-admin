//
//  UIColor+PRVStyle.m
//  FitnessApp
//
//  Created by Ruslan on 8/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UIColor+PRVStyle.h"

@implementation UIColor (PRVStyle)

+ (UIColor *)backgroundColor{
    return [UIColor colorWithRed:0.953 green:0.953 blue:0.953 alpha:1.00];
}

+ (UIColor *)primaryColor{
    return [UIColor colorWithRed:0.388 green:0.788 blue:0.380 alpha:1.00];
}

+ (UIColor *)primaryColorDark{
    return [UIColor colorWithRed:0.325 green:0.647 blue:0.318 alpha:1.00];
}

+ (UIColor *)primaryColorLight{
    return [UIColor colorWithRed:0.471 green:0.898 blue:0.451 alpha:1.00];
}

+ (UIColor *)secondPrimaryColor{
    return [UIColor colorWithRed:0.984 green:0.706 blue:0.475 alpha:1.00];
}

+ (UIColor *)secondPrimaryColorDark{
    return [UIColor colorWithRed:0.639 green:0.376 blue:0.153 alpha:1.00];
}

+ (UIColor *)secondPrimaryColorLight{
    return [UIColor colorWithRed:0.992 green:0.780 blue:0.608 alpha:1.00];
}

+ (UIColor *)textColor{
    return [UIColor blackColor];
}

+ (UIColor *)secondTextColor{
    return [UIColor colorWithRed:0.529 green:0.525 blue:0.525 alpha:1.00];
}

+ (UIColor *)secondColor{
    return [UIColor colorWithRed:0.482 green:0.365 blue:1.000 alpha:1.00];
}

+ (UIColor *)secondColorDark{
    return [UIColor colorWithRed:0.251 green:0.188 blue:0.522 alpha:1.00];
}

+ (UIColor *)secondColorLight{
    return [UIColor colorWithRed:0.443 green:0.427 blue:1.000 alpha:1.00];
}

+ (UIColor *)thirdColor{
    return [UIColor colorWithRed:0.984 green:0.475 blue:0.475 alpha:1.00];
}

+ (UIColor *)thirdColorDark{
    return [UIColor colorWithRed:0.639 green:0.153 blue:0.153 alpha:1.00];
}

+ (UIColor *)thirdColorLight{
    return [UIColor colorWithRed:0.992 green:0.608 blue:0.608 alpha:1.00];
}

@end
