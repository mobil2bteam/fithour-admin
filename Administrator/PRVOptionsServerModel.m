//
//  PRVOptionsServerModel.m
//  FitnessApp
//
//  Created by Ruslan on 7/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVOptionsServerModel.h"
#import "PRVCache+CoreDataProperties.h"
#import "NSString+PRVExtension.h"

@implementation PRVOptionsServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVUrlServerModel class] forKeyPath:@"options.urls" forProperty:@"urls"];
    }];
}

- (instancetype)initWithCache:(PRVCache *)cache{
    self = [super init];
    if (self) {
        NSDictionary *optionsData = [cache.json prv_dictionaryValue];
        self = [EKMapper objectFromExternalRepresentation:optionsData withMapping:[PRVOptionsServerModel objectMapping]];
    }
    return self;
}

- (PRVUrlServerModel *)urlForName:(NSString *)name{
    for (PRVUrlServerModel *url in self.urls) {
        if ([url.name isEqualToString:name]) {
            return url;
        }
    }
    return nil;
}

@end


@implementation PRVUrlServerModel

+(EKObjectMapping *)objectMapping
{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"url", @"metod"]];
    }];
}

@end

@implementation PRVGroupServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVProgramServerModel class] forKeyPath:@"programs"];
        [mapping mapPropertiesFromArray:@[@"name", @"icon", @"icon_select", @"images"]];
        [mapping mapKeyPath:@"description" toProperty:@"_description"];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

@end


@implementation PRVProgramServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"images", @"free"]];
        [mapping mapKeyPath:@"description" toProperty:@"_description"];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping hasMany:[PRVScheduleServerModel class] forKeyPath:@"schedule" forProperty:@"schedule"];
    }];
}

- (NSInteger)minimalPrice{
    NSInteger price = 0;
    for (PRVScheduleServerModel *shedule in self.schedule) {
        if (shedule.price) {
            return shedule.price;
        }
    }
    return price;
}

- (NSInteger)duration{
    for (PRVScheduleServerModel *shedule in self.schedule) {
        for (PRVHourServerModel *hour in shedule.hours) {
            if (hour.duration) {
                return hour.duration;
            }
        }
    }
    return 0;
}

- (NSInteger)price{
    for (PRVScheduleServerModel *shedule in self.schedule) {
        if (shedule.price) {
            return shedule.price;
        }
    }
    return 0;
}

@end


@implementation PRVScheduleServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"date", @"week", @"price", @"free"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping hasMany:[PRVHourServerModel class] forKeyPath:@"hours" forProperty:@"hours"];
        [mapping hasMany:[PRVTimeServerModel class] forKeyPath:@"time" forProperty:@"times"];
    }];
}

- (NSString *)formattedDay{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"d.MM.y";
    
    NSDate *date = [formatter dateFromString:self.date];
    NSLog(@"date - %@", self.date);
    NSDateFormatter *dayNameFormatter = [[NSDateFormatter alloc] init];
    [dayNameFormatter setDateFormat:@"EE"];

    return [[dayNameFormatter stringFromDate:date] uppercaseString];
}

- (NSString *)formattedTime{
    NSMutableArray <NSString *> *times = [[NSMutableArray alloc] init];
    for (PRVHourServerModel *time in self.hours) {
        [times addObject:time.from];
    }
    if (times.count > 2) {
        return [NSString stringWithFormat:@"%@...", [[times subarrayWithRange:NSMakeRange(0, 2)]componentsJoinedByString:@","]];
    } else {
        return [times componentsJoinedByString:@","];
    }
}

@end


@implementation PRVHourServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"from", @"to", @"places", @"duration", @"coachId"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

@end


@implementation PRVTimeServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"flag", @"value"]];
    }];
}

@end

@implementation PRVCityServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"regionId"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeInteger:self.regionId forKey:@"regionId"];
    [aCoder encodeInteger:self._id forKey:@"_id"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        self.name = [decoder decodeObjectForKey:@"name"];
        self._id = [decoder decodeIntegerForKey:@"_id"];
        self.regionId = [decoder decodeIntegerForKey:@"regionId"];
    }
    return self;
}

@end

@implementation PRVPriceServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"text", @"value"]];
    }];
}

@end

