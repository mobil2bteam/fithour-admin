//
//  UIColor+PRVStyle.h
//  FitnessApp
//
//  Created by Ruslan on 8/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PRVStyle)

+ (UIColor *)backgroundColor;

+ (UIColor *)primaryColor;
+ (UIColor *)primaryColorDark;
+ (UIColor *)primaryColorLight;

+ (UIColor *)secondPrimaryColor;
+ (UIColor *)secondPrimaryColorDark;
+ (UIColor *)secondPrimaryColorLight;

+ (UIColor *)textColor;
+ (UIColor *)secondTextColor;

// фиолетовый
+ (UIColor *)secondColor;
+ (UIColor *)secondColorDark;
+ (UIColor *)secondColorLight;

// розовый
+ (UIColor *)thirdColor;
+ (UIColor *)thirdColorDark;
+ (UIColor *)thirdColorLight;

@end
