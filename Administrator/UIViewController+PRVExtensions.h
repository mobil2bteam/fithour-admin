//
//  UIViewController+PRVExtensions.h
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (PRVExtensions)

- (instancetype)prv_initWithNib;

- (void)prv_addMBProgressHUD;

- (void)prv_hideMBProgressHUD;

- (void)prv_showMessage:(NSString *)message withTitle:(NSString *)title;

- (void)prv_showMessage:(NSString *)message
      withOkHandler:(void (^)(UIAlertAction *action))okHandler;

- (void)prv_showMessage:(NSString *)message
      withOkHandler:(void (^)(UIAlertAction *action))okHandler
   andRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler;

- (void)prv_showMessage:(NSString *)message
  withRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler
     andBackHandler:(void (^)(UIAlertAction *action))backHandler;

- (void)prv_showMessage:(NSString *)message
  withRepeatHandler:(void (^)(UIAlertAction *action))repeatHandler;

@end
