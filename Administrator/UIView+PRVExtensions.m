//
//  UIView+PRVExtensions.m
//  FitnessApp
//
//  Created by Ruslan on 8/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "UIView+PRVExtensions.h"

@implementation UIView (PRVExtensions)

- (void)prv_shake{
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.05];
    [animation setRepeatCount:2];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([self center].x - 20.0f, [self center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([self center].x + 20.0f, [self center].y)]];
    [[self layer] addAnimation:animation forKey:@"position"];
}

@end
