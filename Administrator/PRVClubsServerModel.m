//
//  PRVClubsServerModel.m
//  FitnessApp
//
//  Created by Ruslan on 7/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubsServerModel.h"
#import "PRVOptionsServerModel.h"

@implementation PRVClubsServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVClubServerModel class] forKeyPath:@"clubs.list" forProperty:@"list"];
        [mapping hasMany:[PRVClubServerModel class] forKeyPath:@"clubs.list_point" forProperty:@"list_point"];
        [mapping mapKeyPath:@"clubs.filter.page" toProperty:@"page"];
        [mapping mapKeyPath:@"clubs.filter.pageCount" toProperty:@"pageCount"];
        [mapping mapKeyPath:@"clubs.filter.count" toProperty:@"count"];
    }];
}

- (void)addClubs:(NSArray <PRVClubServerModel *> *)clubs{
    self.list = [self.list arrayByAddingObjectsFromArray:clubs];
}

@end


@implementation PRVClubServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVReviewServerModel class] forKeyPath:@"reviews.other" forProperty:@"reviews"];
        [mapping hasOne:[PRVReviewServerModel class] forKeyPath:@"reviews.user" forProperty:@"user_review"];
        [mapping hasMany:[PRVGroupServerModel class] forKeyPath:@"groups" forProperty:@"groups"];
        [mapping hasMany:[PRVPhoneServerModel class] forKeyPath:@"phones" forProperty:@"phones"];
        [mapping hasMany:[PRVMetroServerModel class] forKeyPath:@"metro" forProperty:@"metro"];
        [mapping hasMany:[PRVCoachServerModel class] forKeyPath:@"coaches" forProperty:@"coaches"];
        [mapping hasMany:[PRVWorkingHourServerModel class] forKeyPath:@"working_house" forProperty:@"working_hours"];
        [mapping hasOne:[PRVWorkingHourServerModel class] forKeyPath:@"work" forProperty:@"work"];
        [mapping mapPropertiesFromArray:@[@"name", @"logo", @"group_id", @"city_id", @"city_name", @"address", @"office", @"lat", @"lng", @"price_free", @"price_minute", @"distance", @"rating", @"images", @"site", @"count_reviews"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping mapKeyPath:@"description" toProperty:@"_description"];
    }];
}

- (NSInteger)minimalPriceForGroups{
    NSInteger minPrice = 0;
    for (PRVGroupServerModel *group in self.groups) {
        for (PRVProgramServerModel *program in group.programs) {
            for (PRVScheduleServerModel *schedule in program.schedule) {
                if (minPrice == 0) {
                    minPrice = schedule.price;
                }
                if (schedule.price < minPrice) {
                    minPrice = schedule.price;
                }
            }
        }
    }
    return minPrice;
}

- (PRVCoachServerModel *)coachById:(NSInteger)coachId{
    if (!coachId) {
        return nil;
    }
    for (PRVCoachServerModel *coach in self.coaches) {
        if (coach._id == coachId) {
            return coach;
        }
    }
    return nil;
}

- (BOOL)hasGroupsWithFreeSchdeule{
    for (PRVGroupServerModel *group in self.groups) {
        for (PRVProgramServerModel *program in group.programs) {
            if (program.free) {
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)areAllGroupsWithFreeSchdeule{
    if (!self.groups.count) {
        NO;
    }
    for (PRVGroupServerModel *group in self.groups) {
        for (PRVProgramServerModel *program in group.programs) {
            if (!program.free) {
                return NO;
            }
        }
    }
    return YES;
}

- (NSArray <PRVGroupServerModel *> *)groupsWithFreeSchedule{
    NSMutableArray <PRVGroupServerModel *> *temp = [[NSMutableArray alloc] init];
    
    for (PRVGroupServerModel *group in self.groups) {
        NSMutableArray<PRVProgramServerModel *> *programs = [[NSMutableArray alloc] init];
        for (PRVProgramServerModel *program in group.programs) {
            if (program.free) {
                [programs addObject:program];
            }
        }
        if (programs.count) {
            PRVGroupServerModel *temp2 = group;
            temp2.programs = programs;
            [temp addObject:temp2];
        }
    }
    return [temp copy];
}

- (NSArray <PRVGroupServerModel *> *)groupsWithoutFreeSchedule{
    NSMutableArray <PRVGroupServerModel *> *temp = [[NSMutableArray alloc] init];
    for (PRVGroupServerModel *group in self.groups) {
        NSMutableArray<PRVProgramServerModel *> *programs = [[NSMutableArray alloc] init];
        for (PRVProgramServerModel *program in group.programs) {
            if (!program.free) {
                [programs addObject:program];
            }
        }
        if (programs.count) {
            PRVGroupServerModel *temp2 = group;
            temp2.programs = programs;
            [temp addObject:temp2];
        }
    }
    return [temp copy];
}

- (PRVWorkingHourServerModel *)workingHoursForDate:(NSDate *)date{
    NSDateFormatter *dayNameFormatter = [[NSDateFormatter alloc] init];
    [dayNameFormatter setDateFormat:@"EEEE"];
    NSString *dayName = [dayNameFormatter stringFromDate:date];
   
    for (PRVWorkingHourServerModel *hours in self.working_hours) {
        if( [hours.name caseInsensitiveCompare:dayName] == NSOrderedSame ) {
            return hours;
        }
    }
    return nil;
}

@end


@implementation PRVReviewServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"rating", @"comment", @"comment_plus", @"comment_minus", @"date"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping mapKeyPath:@"user.name" toProperty:@"user"];
    }];
}

@end


@implementation PRVMetroServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"city_id", @"city_name", @"name", @"color", @"line"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

@end


@implementation PRVCoachServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"rank", @"price_hour", @"name", @"image"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping mapKeyPath:@"description" toProperty:@"_description"];
        [mapping hasMany:[PRVGroupServerModel class] forKeyPath:@"groups" forProperty:@"groups"];
    }];
}

@end


@implementation PRVWorkingHourServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"hours_start", @"hours_finish", @"free_day"]];
    }];
}

@end

@implementation PRVPhoneServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"name", @"phone"]];
    }];
}

@end

