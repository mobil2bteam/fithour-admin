//
//  PRVTicketActionAlertViewController.m
//  Administrator
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketActionAlertViewController.h"

@interface PRVTicketActionAlertViewController ()

@end

@implementation PRVTicketActionAlertViewController

#pragma mark - Actions

- (IBAction)dismissTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)listButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        if (strongSelf.block) {
            strongSelf.block(NO);
        }
    }];
}

- (IBAction)scanButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        if (strongSelf.block) {
            strongSelf.block(YES);
        }
    }];
}

@end
