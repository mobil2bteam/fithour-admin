//
//  PRVTicketDetailViewController.m
//  Administrator
//
//  Created by Ruslan on 9/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketDetailViewController.h"
#import "PRVCoachImageView.h"
#import "PRVUserServerModel.h"
#import "PRVClubsServerModel.h"
#import "UIImageView+PRVExtension.h"
#import "PRVOptionsServerModel.h"
#import "PRVServerManager.h"
#import "PRVPaymentAlertViewController.h"
#import "PRVVerificationViewController.h"
#import "PRVTicketFinishViewController.h"
#import "PRVAppManager.h"
#import "PRVProgressView.h"
#import "MWPhotoBrowser.h"

#import "AFNetworking.h"
#import "AFHTTPSessionOperation.h"
#import "PRVAppManager.h"
#import <MBProgressHUD/MBProgressHUD.h>

@interface PRVTicketDetailViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, MWPhotoBrowserDelegate>
@property (weak, nonatomic) IBOutlet UIView *coachView;
@property (weak, nonatomic) IBOutlet UIImageView *programImageView;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;
@property (weak, nonatomic) IBOutlet PRVCoachImageView *coachImageView;
@property (weak, nonatomic) IBOutlet UILabel *coachNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *coachRankLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *ticketStatusLabel;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *payButton;
@property (weak, nonatomic) IBOutlet UIView *timeStartView;
@property (weak, nonatomic) IBOutlet UILabel *timeStartLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeFinishLabel;
@property (weak, nonatomic) IBOutlet UIView *timeFinishView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *ticketNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *clubNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *verifiedView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userPhoneLabel;
@property (weak, nonatomic) IBOutlet UIView *additionalTimeView;
@property (weak, nonatomic) IBOutlet UILabel *failMinutesLabel;
@property (weak, nonatomic) IBOutlet UILabel *failPriceLabel;
@property (weak, nonatomic) IBOutlet PRVProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIView *activateView;
@property (strong, nonatomic) NSArray *photos;
@end

@implementation PRVTicketDetailViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Билет";
    [self prepareView];
}

- (void)prepareView{
    [self.progressView prepareViewForTicket:self.ticket];
    self.payButton.hidden = self.ticket.isPayment;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.ticket.price];
    self.timeLabel.text = [NSString stringWithFormat:@"%@ в %@ до %@", self.ticket.date, self.ticket.time_from, self.ticket.time_to];
    self.ticketNumberLabel.text = [NSString stringWithFormat:@"№%@", self.ticket.code];
    self.clubNameLabel.text = self.ticket.club.name;
    self.addressLabel.text = self.ticket.club.address;
    self.userNameLabel.text = self.ticket.user.info.name;
    self.userPhoneLabel.text = self.ticket.user.info.phone;
    if (self.ticket.enrollmentType == PRVEnrollTicketTypeDefault) {
        self.programLabel.text = [self.ticket.program.name uppercaseString];
        [self.programImageView prv_setImageWithURL:self.ticket.program.images[0]];
    }
    if (self.ticket.enrollmentType == PRVEnrollTicketTypePersonalCoach) {
        self.programLabel.text = @"ПЕРСОНАЛЬНАЯ ТРЕНИРОВКА";
        if (self.ticket.coach.image.length) {
            [self.programImageView prv_setImageWithURL:self.ticket.coach.image];
        } else {
            self.programImageView.image = [UIImage imageNamed:@"coach_placeholder.jpg"];
        }
    }
    if (self.ticket.enrollmentType == PRVEnrollTicketTypeFreeSchedule) {
        self.programLabel.text = @"СВОБОДНАЯ ЗОНА";
        self.programImageView.image = [UIImage imageNamed:@"free_zone.jpg"];
    }

    if (self.ticket.isPayment) {
        self.ticketStatusLabel.text = @"Оплачен";
        self.ticketStatusLabel.textColor = [UIColor primaryColor];
    } else {
        self.ticketStatusLabel.text = @"Не оплачен";
        self.ticketStatusLabel.textColor = [UIColor thirdColorDark];
    }
    
    if ([self.ticket isCompleted]) {
        self.activateView.hidden = YES;
        self.timeFinishLabel.text = self.ticket.timeComplete;
        self.timeFinishView.hidden = !self.ticket.timeComplete.length;
    } else {
        self.timeFinishView.hidden = YES;
    }
    if ([self.ticket isActive]) {
        self.timeStartLabel.text = self.ticket.timeActive;
        self.timeStartView.hidden = NO;
    } else {
        self.timeStartView.hidden = YES;
    }
    
    if ([self.ticket isActive]) {
        [self.actionButton setTitle:@"Завершить" forState:UIControlStateNormal];
        self.actionButton.backgroundColor = [UIColor secondColor];
    }
    
    // If user used additional minutes
    if (self.ticket.failMinutes) {
        self.failMinutesLabel.text = [NSString stringWithFormat:@"%ld мин.", (long)self.ticket.failMinutes];
        self.failPriceLabel.text = [NSString stringWithFormat:@"%ld руб.", (long)self.ticket.failPrice];
        self.additionalTimeView.hidden = NO;
    } else {
        self.additionalTimeView.hidden = YES;
    }

    if (self.ticket.coach) {
        [self.coachImageView prv_setImageWithURL:self.ticket.coach.image];
        self.coachNameLabel.text = self.ticket.coach.name;
        self.coachRankLabel.text = self.ticket.coach.rank;
        self.coachView.hidden = NO;
    }
    
    if (self.ticket.program.images.count) {
        [self.programImageView prv_setImageWithURL:self.ticket.program.images[0]];
    }
    
    self.verifiedView.hidden = self.ticket.user.info.verify ? NO : YES;
}

- (void)showImageForUrl:(NSString *)url {
    self.photos = @[[MWPhoto photoWithURL:[NSURL URLWithString:url]]];

    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = NO; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.enableSwipeToDismiss = NO;
    
    UINavigationController *navVC = [[UINavigationController alloc]initWithRootViewController:browser];
    [self presentViewController:navVC animated:NO completion:nil];
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count) {
        return [self.photos objectAtIndex:index];
    }
    return nil;
}

#pragma mark - Actions

- (IBAction)photoPressed:(id)sender {
    if (self.ticket.user.info.image != nil) {
        [self showImageForUrl:self.ticket.user.info.image];
    }
}

- (IBAction)passportPressed:(id)sender {
    if (self.ticket.user.info.image2 != nil) {
        [self showImageForUrl:self.ticket.user.info.image2];
    }
}

- (IBAction)actionButtonPressed:(id)sender {
    if ([self.ticket isPayment]) {
        if (self.ticket.user.info.verify) {
            // if ticket is already activated, than finish it
            if ([self.ticket isActive]) {
                [self updateTicket];
            } else {
                // check time
                // user can activate ticket not later than 15 minutes before start time
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"d.MM.y HH:mm"];
                NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.ticket.date, self.ticket.time_from];
                NSDate *startTrainingDate = [formatter dateFromString:dateAndTimeString];
                NSTimeInterval substraction = [startTrainingDate timeIntervalSinceDate:[NSDate date]];
                if (substraction > 0) {
                    if (substraction < 15 * 60) {
                        [self updateTicket];
                    } else {
                        NSString *message = [NSString stringWithFormat:@"Вы можете войти не раньше, чем за 15 минут до начала (%@)", dateAndTimeString];
                        [self prv_showMessage:message withTitle:nil];
                    }
                } else {
                    __weak typeof(self) weakSelf = self;
                    NSString *message = [NSString stringWithFormat:@"Вы опоздали. Время начала - %@", dateAndTimeString];
                    [self prv_showMessage:message withOkHandler:^(UIAlertAction *action) {
                        typeof(self) strongSelf = weakSelf;
                        [strongSelf updateTicket];
                    }];
                }
            }
        } else {
            // verify user
            [self startVerification];
        }
    } else {
        __weak typeof(self) weakSelf = self;
        PRVPaymentAlertViewController *vc = [[PRVPaymentAlertViewController alloc] prv_initWithNib];
        vc.paymentBlock = ^{
            typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf payTicket];
        };
        vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:vc animated:YES completion:nil];
    }
}

- (void)startVerification{
    __weak typeof(self) weakSelf = self;
    PRVVerificationViewController *vc = [[PRVVerificationViewController alloc] prv_initWithNib];
    vc.verificationBlock = ^{
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf verifyUser];
    };
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

// this method changes ticket state. If ticket isn't active than activate, otherwise finish ticket
- (void)updateTicket{
    __weak typeof(self) weakSelf = self;
    BOOL isEnter = ![self.ticket isActive];
    [self prv_addMBProgressHUD];
    [[PRVServerManager sharedManager] updateTicketWithCode:self.ticket.code enter:isEnter onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        if (ticket) {
            [[PRVAppManager sharedInstance].getUser updateTicket:ticket];
            strongSelf.ticket = ticket;
            [strongSelf prepareView];
            if (ticket.user.info.verify == NO) {
                [strongSelf startVerification];
            }
        } else {
            [self prv_showMessage:error withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self prv_hideMBProgressHUD];
        [self prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

- (void)payTicket{
    __weak typeof(self) weakSelf = self;
    [self prv_addMBProgressHUD];
    [[PRVServerManager sharedManager] payTicketWithCode:self.ticket.code onSuccess:^(PRVTicketSeverModel *ticket, NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        if (ticket) {
            [[PRVAppManager sharedInstance].getUser updateTicket:ticket];
            strongSelf.ticket = ticket;
            [strongSelf prepareView];
            if (ticket.user.info.verify) {
                [strongSelf updateTicket];
            } else {
                [strongSelf startVerification];
            }
        } else {
            [strongSelf prv_showMessage:error withTitle:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        [strongSelf prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

- (void)verifyUser{
    [self uplodeImage:nil];
}

- (void)takePhoto{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.allowsEditing = NO;
        [self presentViewController:picker animated:YES completion:nil];
    } else {
        [self prv_showMessage:@"Камера не доступна на вашем устройстве" withTitle:nil];
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    UIImage *pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (pickedImage) {
        [self uplodeImage:pickedImage];
    }
}

-(void)uplodeImage:(UIImage *)image{
//    NSData *imageData =  UIImagePNGRepresentation(image);  // convert your image into data

    NSString *token = [[PRVAppManager sharedInstance] getUser].info.accessToken;
    NSString *userID = [NSString stringWithFormat:@"%ld", (long)self.ticket.user.info._id];

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];  // allocate AFHTTPManager
    [self prv_addMBProgressHUD];
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_user_verify"];

    [manager POST:url.url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
//        [formData appendPartWithFileData:imageData name:@"image" fileName:@"example.jpg" mimeType:@"image/jpeg"];
        [formData appendPartWithFormData:[token dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"token"];
        [formData appendPartWithFormData:[userID dataUsingEncoding:NSUTF8StringEncoding]
                                    name:@"user_id"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    } success:^(NSURLSessionDataTask *task, id responseObject) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self prv_hideMBProgressHUD];
        });
        if (responseObject[@"user"]) {
            self.ticket.user = [EKMapper objectFromExternalRepresentation:responseObject[@"user"] withMapping:[PRVUserServerModel objectMapping]];
            [self prepareView];
        } else {
            [self prv_showMessage:@"Error" withTitle:nil];
        }
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self prv_hideMBProgressHUD];
        });
    }];
}

@end
