//
//  PRVLogoutViewController.m
//  Administrator
//
//  Created by Ruslan on 13.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVLogoutViewController.h"
@implementation PRVLogoutViewController

#pragma mark - Actions

- (IBAction)yesButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        if (strongSelf.logoutBlock) {
            strongSelf.logoutBlock();
        }
    }];
}

- (IBAction)noButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dismissTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
