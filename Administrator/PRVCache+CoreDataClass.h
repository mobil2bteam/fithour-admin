//
//  PRVCache+CoreDataClass.h
//  
//
//  Created by Ruslan on 7/11/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface PRVCache : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "PRVCache+CoreDataProperties.h"
