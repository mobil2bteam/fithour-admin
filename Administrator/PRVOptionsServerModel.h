//
//  PRVOptionsServerModel.h
//  FitnessApp
//
//  Created by Ruslan on 7/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVUrlServerModel;
@class PRVGroupServerModel;
@class PRVProgramServerModel;
@class PRVCityServerModel;
@class PRVPriceServerModel;
@class PRVScheduleServerModel;
@class PRVHourServerModel;
@class PRVTimeServerModel;
@class PRVCache;

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface PRVOptionsServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, strong) NSArray<PRVUrlServerModel *> *urls;

- (instancetype)initWithCache:(PRVCache *)cache;
+ (EKObjectMapping *)objectMapping;
- (PRVUrlServerModel *)urlForName:(NSString *)name;
@end


@interface PRVUrlServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *metod;
@end


@interface PRVGroupServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *_description;
@property (nonatomic, copy) NSString *icon;
@property (nonatomic, copy) NSString *icon_select;
@property (nonatomic, assign) NSInteger _id;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSArray<PRVProgramServerModel *> *programs;
@end


@interface PRVProgramServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *_description;
@property (nonatomic, assign) NSInteger _id;
@property (nonatomic, assign) BOOL free;
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, strong) NSArray<PRVScheduleServerModel *> *schedule;

- (NSInteger)duration;
- (NSInteger)price;
@end


@interface PRVScheduleServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *week;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, strong) NSArray<PRVHourServerModel *> *hours;
@property (nonatomic, strong) NSArray<PRVTimeServerModel *> *times;

- (NSString *)formattedDay;
- (NSString *)formattedTime;
@end


@interface PRVHourServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, assign) NSInteger coachId;
@property (nonatomic, assign) NSInteger _id;
@property (nonatomic, assign) NSInteger places;
@property (nonatomic, copy) NSString *from;
@property (nonatomic, copy) NSString *to;
@property (nonatomic, assign) NSInteger duration;
@end

@interface PRVTimeServerModel : NSObject <EKMappingProtocol>
@property (nonatomic, assign) BOOL flag;
@property (nonatomic, copy) NSString *value;
@end


@interface PRVCityServerModel : NSObject <EKMappingProtocol, NSCoding>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger _id;
@property (nonatomic, assign) NSInteger regionId;
@end


@interface PRVPriceServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *text;
@property (nonatomic, assign) NSInteger value;

@end
