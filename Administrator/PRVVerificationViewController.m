//
//  PRVVerificationViewController.m
//  Administrator
//
//  Created by Ruslan on 13.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVVerificationViewController.h"

@interface PRVVerificationViewController ()

@end

@implementation PRVVerificationViewController

- (IBAction)takePhotoButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        if (strongSelf.verificationBlock) {
            strongSelf.verificationBlock();
        }
    }];
}
- (IBAction)noButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dismissTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
