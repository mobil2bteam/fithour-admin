//
//  PRVPaymentAlertViewController.m
//  Administrator
//
//  Created by Ruslan on 9/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVPaymentAlertViewController.h"

@interface PRVPaymentAlertViewController ()

@end

@implementation PRVPaymentAlertViewController

#pragma mark - Actions

- (IBAction)yesButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        if (strongSelf.paymentBlock) {
            strongSelf.paymentBlock();
        }
    }];
}

- (IBAction)noButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dismissTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
