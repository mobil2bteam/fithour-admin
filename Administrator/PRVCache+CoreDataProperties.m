//
//  PRVCache+CoreDataProperties.m
//  
//
//  Created by Ruslan on 7/11/17.
//
//

#import "PRVCache+CoreDataProperties.h"

@implementation PRVCache (CoreDataProperties)

+ (NSFetchRequest<PRVCache *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"PRVCache"];
}

@dynamic date;
@dynamic json;
@dynamic key;
@dynamic params;
@dynamic seconds;

@end
