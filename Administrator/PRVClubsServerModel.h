//
//  PRVClubsServerModel.h
//  FitnessApp
//
//  Created by Ruslan on 7/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

@class PRVClubServerModel;
@class PRVReviewServerModel;
@class PRVMetroServerModel;
@class PRVCoachServerModel;
@class PRVProgramServerModel;
@class PRVGroupServerModel;
@class PRVWorkingHourServerModel;
@class PRVPhoneServerModel;

#import <Foundation/Foundation.h>
#import "EasyMapping/EasyMapping.h"

@interface PRVClubsServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, strong) NSArray<PRVClubServerModel *> *list;

@property (nonatomic, strong) NSArray<PRVClubServerModel *> *list_point;

@property (nonatomic, assign) NSInteger pageCount;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, assign) NSInteger count;


+ (EKObjectMapping *)objectMapping;

- (void)addClubs:(NSArray <PRVClubServerModel *> *)clubs;

@end

@interface PRVClubServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *_description;

@property (nonatomic, copy) NSString *logo;

@property (nonatomic, assign) NSInteger _id;

@property (nonatomic, assign) NSInteger group_id;

@property (nonatomic, assign) NSInteger city_id;

@property (nonatomic, assign) NSInteger count_reviews;

@property (nonatomic, copy) NSString *city_name;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *office;

@property (nonatomic, assign) CGFloat lat;

@property (nonatomic, assign) CGFloat lng;

@property (nonatomic, assign) NSInteger price_free;

@property (nonatomic, assign) NSInteger price_minute;

@property (nonatomic, assign) NSInteger distance;

@property (nonatomic, assign) CGFloat rating;

@property (nonatomic, copy) NSString *site;

@property (nonatomic, copy) NSArray *images;

@property (nonatomic, strong) NSArray<PRVReviewServerModel *> *reviews;

@property (nonatomic, strong) PRVReviewServerModel *user_review;

@property (nonatomic, strong) NSArray<PRVGroupServerModel *> *groups;

@property (nonatomic, strong) NSArray<PRVMetroServerModel *> *metro;

@property (nonatomic, strong) NSArray<PRVCoachServerModel *> *coaches;

@property (nonatomic, strong) NSArray<PRVWorkingHourServerModel *> *working_hours;

@property (nonatomic, strong) NSArray<PRVPhoneServerModel *> *phones;

@property (nonatomic, strong) PRVWorkingHourServerModel *work;

+ (EKObjectMapping *)objectMapping;

- (NSInteger)minimalPriceForGroups;

- (PRVCoachServerModel *)coachById:(NSInteger)coachId;

- (BOOL)hasGroupsWithFreeSchdeule;

- (BOOL)areAllGroupsWithFreeSchdeule;

- (NSArray <PRVGroupServerModel *> *)groupsWithFreeSchedule;

- (NSArray <PRVGroupServerModel *> *)groupsWithoutFreeSchedule;

- (PRVWorkingHourServerModel *)workingHoursForDate:(NSDate *)date;

@end


@interface PRVReviewServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) CGFloat rating;

@property (nonatomic, assign) NSInteger _id;

@property (nonatomic, copy) NSString *date;

@property (nonatomic, copy) NSString *user;

@property (nonatomic, copy) NSString *comment;

@property (nonatomic, copy) NSString *comment_plus;

@property (nonatomic, copy) NSString *comment_minus;

+ (EKObjectMapping *)objectMapping;

@end


@interface PRVMetroServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger city_id;

@property (nonatomic, assign) NSInteger _id;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *city_name;

@property (nonatomic, copy) NSString *color;

@property (nonatomic, copy) NSString *line;

+ (EKObjectMapping *)objectMapping;

@end


@interface PRVCoachServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) NSInteger price_hour;

@property (nonatomic, assign) NSInteger _id;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *_description;

@property (nonatomic, copy) NSString *rank;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, strong) NSArray<PRVGroupServerModel *> *groups;

+ (EKObjectMapping *)objectMapping;

@end


@interface PRVWorkingHourServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, assign) BOOL free_day;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *hours_start;

@property (nonatomic, copy) NSString *hours_finish;

+ (EKObjectMapping *)objectMapping;

@end


@interface PRVPhoneServerModel : NSObject <EKMappingProtocol>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *phone;

+ (EKObjectMapping *)objectMapping;

@end
