//
//  PRVTicketFinishViewController.h
//  Administrator
//
//  Created by Ruslan on 9/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PRVTicketSeverModel;
@interface PRVTicketFinishViewController : UIViewController
@property (nonatomic, strong) PRVTicketSeverModel *ticket;
@end
