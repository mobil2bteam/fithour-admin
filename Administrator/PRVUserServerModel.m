//
//  PRVUserServerModel.m
//  FitnessApp
//
//  Created by Ruslan on 7/19/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVUserServerModel.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"

static const NSInteger kMinutesToExit = 15;

@implementation PRVUserServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasOne:[PRVInfoServerModel class] forKeyPath:@"info" forProperty:@"info"];
    }];
}

- (PRVTicketSeverModel *)ticketForCode:(NSString *)code{
    for (PRVTicketSeverModel *ticket in self.info.tickets) {
        if ([ticket.code isEqualToString:code]) {
            return ticket;
        }
    }
    return nil;
}

- (void)updateTicket:(PRVTicketSeverModel *)updatedTicket{
    PRVTicketSeverModel *oldTicket = [self ticketForCode:updatedTicket.code];
    if (oldTicket) {
        NSInteger index = [self.info.tickets indexOfObject:oldTicket];
        NSMutableArray *temp = [self.info.tickets mutableCopy];
        temp[index] = updatedTicket;
        self.info.tickets = [temp copy];
    }
}

- (NSArray <PRVTicketSeverModel *> *)ticketsForEnter{
    NSMutableArray <PRVTicketSeverModel *> *temp = [[NSMutableArray alloc] init];
    for (PRVTicketSeverModel *ticket in self.info.tickets) {
        if ([ticket isCanceled] || [ticket isCompleted]) {
            continue;
        }
        if (![ticket isActive]) {
            [temp addObject:ticket];
        }
    }
    return [temp copy];
}

- (NSArray <PRVTicketSeverModel *> *)ticketsForExit{
    NSMutableArray <PRVTicketSeverModel *> *temp = [[NSMutableArray alloc] init];
    for (PRVTicketSeverModel *ticket in self.info.tickets) {
        if ([ticket isCanceled] || [ticket isCompleted]) {
            continue;
        }
        if ([ticket isActive]) {
            [temp addObject:ticket];
        }
    }
    return [temp copy];
}

@end


@implementation PRVInfoServerModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping hasMany:[PRVTicketSeverModel class] forKeyPath:@"tickets" forProperty:@"tickets"];
        [mapping hasOne:[PRVClubServerModel class] forKeyPath:@"club" forProperty:@"club"];

        [mapping mapPropertiesFromArray:@[@"accessToken", @"image", @"image2", @"email", @"balance", @"name", @"phone", @"verify"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
    }];
}

- (NSArray <PRVTicketSeverModel *> *)tickets;{
    NSMutableArray <PRVTicketSeverModel *> *temp = [[NSMutableArray alloc] init];
    for (PRVTicketSeverModel *ticket in _tickets) {
        if (![ticket isCompleted] && ![ticket isCanceled]) {
            [temp addObject:ticket];
        }
    }
    return temp;
}

@end

@implementation PRVTicketSeverModel

+ (EKObjectMapping *)objectMapping{
    return [EKObjectMapping mappingForClass:self withBlock:^(EKObjectMapping *mapping) {
        [mapping mapPropertiesFromArray:@[@"date", @"dateActive", @"dateComplete", @"dateCancel", @"datePayment", @"free", @"code", @"price", @"failMinutes", @"failPrice", @"isPayment", @"time_from", @"time_to", @"timeActive", @"timeCancel", @"timeComplete", @"timePayment", @"totalPrice", @"wallet", @"workMinutes"]];
        [mapping mapKeyPath:@"id" toProperty:@"_id"];
        [mapping hasOne:[PRVClubServerModel class] forKeyPath:@"club" forProperty:@"club"];
        [mapping hasOne:[PRVUserServerModel class] forKeyPath:@"user" forProperty:@"user"];
        [mapping hasOne:[PRVCoachServerModel class] forKeyPath:@"coach" forProperty:@"coach"];
        [mapping hasOne:[PRVProgramServerModel class] forKeyPath:@"program" forProperty:@"program"];
    }];
}

- (BOOL)isActive{
    return self.dateActive.length && !self.dateComplete.length && !self.dateCancel.length;
}

- (BOOL)isNew{
    return !self.dateActive.length && !self.dateComplete.length && !self.dateCancel.length;
}

- (BOOL)isCompleted{
    return self.dateComplete.length || self.dateCancel.length;
}

- (BOOL)isCanceled{
    return self.dateCancel.length;
}

- (PRVEnrollTicketType)enrollmentType{
    if (self.free && !self.coach) {
        return PRVEnrollTicketTypeFreeSchedule;
    }
    if (self.free && self.coach) {
        return PRVEnrollTicketTypePersonalCoach;
    }
    return PRVEnrollTicketTypeDefault;
}

- (NSInteger)trainingDurationInSeconds{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@",self.date, self.time_to];
    NSString *dateAndTimeStartString = [NSString stringWithFormat:@"%@ %@",self.date, self.time_from];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSDate *startTrainingDate = [formatter dateFromString:dateAndTimeStartString];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:startTrainingDate];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    NSInteger seconds = (NSInteger)substraction % 60;
    return hours * 3600 + minutes * 60 + seconds;
}

- (NSInteger)secondsToFinishTraining{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:[NSDate date]];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    NSInteger seconds = (NSInteger)substraction % 60;
    return hours * 3600 + minutes * 60 + seconds;
}

- (NSInteger)minutesToFinishTraining{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:[NSDate date]];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    return minutes + hours * 60;
}

- (NSInteger)minutesToStartTraining{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_from];
    NSDate *startTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSTimeInterval substraction = [startTrainingDate timeIntervalSinceDate:[NSDate date]];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    return minutes + hours * 60;
}

- (NSString *)dateToFinishTraining{
    NSInteger secondsToFinish = [self secondsToFinishTraining];
    NSInteger hours = secondsToFinish / 3600;
    NSInteger minutes = (secondsToFinish / 60) % 60;
    NSInteger seconds = secondsToFinish % 60;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (hours) {
        [formatter setDateFormat:@"HH:mm:ss"];
        return [NSString stringWithFormat:@"%ld:%ld:%ld", (long)hours, (long)minutes, (long)seconds];
    } else {
        [formatter setDateFormat:@"mm:ss"];
        return [NSString stringWithFormat:@"%ld:%ld", (long)minutes, (long)seconds];
    }
}

- (BOOL)isStarted{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_from];
    NSDate *startTrainingDate = [formatter dateFromString:dateAndTimeString];
    return [[NSDate date] compare:startTrainingDate] == NSOrderedDescending;
}

- (BOOL)isEnded{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    return [[NSDate date] compare:finishTrainingDate] == NSOrderedDescending;
}

- (BOOL)isFailTimeStarted{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    finishTrainingDate = [finishTrainingDate dateByAddingTimeInterval:kMinutesToExit * 60];
    return [[NSDate date] compare:finishTrainingDate] == NSOrderedDescending;
}

- (NSInteger)currentFailMinutes{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    finishTrainingDate = [finishTrainingDate dateByAddingTimeInterval:kMinutesToExit * 60];
    NSTimeInterval substraction = [[NSDate date] timeIntervalSinceDate:finishTrainingDate];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    return minutes + hours * 60 + 1;
}

- (NSInteger)currentMinutesToExit{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:[NSDate date]];
    NSInteger hours = substraction / 3600;
    NSInteger minutes = ((NSInteger)substraction / 60) % 60;
    return (minutes + hours * 60) + kMinutesToExit;
}

- (NSInteger)totalSecondsToExit{
    return kMinutesToExit * 60;
}

- (NSInteger)currentSecondsToExit{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"d.MM.y HH:mm"];
    NSString *dateAndTimeString = [NSString stringWithFormat:@"%@ %@", self.date, self.time_to];
    NSDate *finishTrainingDate = [formatter dateFromString:dateAndTimeString];
    finishTrainingDate = [finishTrainingDate dateByAddingTimeInterval:kMinutesToExit * 60];
    NSTimeInterval substraction = [finishTrainingDate timeIntervalSinceDate:[NSDate date]];
    return substraction;
}

@end

