//
//  PRVTicketFinishViewController.m
//  Administrator
//
//  Created by Ruslan on 9/21/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVTicketFinishViewController.h"
#import "PRVUserServerModel.h"

@interface PRVTicketFinishViewController ()
@property (nonatomic, assign) IBOutlet UILabel *statusLabel;
@end

@implementation PRVTicketFinishViewController

#pragma mark - Lifecycle

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([self.ticket isCompleted]) {
        self.statusLabel.text = @"Билет завершен";
    } else {
        self.statusLabel.text = @"Билет активирован";
    }
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

#pragma mark - Actions

- (IBAction)okButtonPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
