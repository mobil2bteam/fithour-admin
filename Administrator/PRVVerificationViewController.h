//
//  PRVVerificationViewController.h
//  Administrator
//
//  Created by Ruslan on 13.11.2017.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVVerificationViewController : UIViewController
@property (nonatomic, copy) void (^verificationBlock)(void);
@end
