//
//  PRVSignInViewController.m
//  Administrator
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVSignInViewController.h"
#import "UIView+PRVExtensions.h"
#import "PRVServerManager.h"
#import "PRVMainViewController.h"
#import "PRVRestorePasswordAlertVC.h"

@interface PRVSignInViewController ()
@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIView *testVeiw;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIView *logoView;
@end

@implementation PRVSignInViewController

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.logoView.maskView = self.logoImageView;

    // add gradient as background
    UIColor *startColor = [UIColor colorWithRed:0.106 green:0.475 blue:0.808 alpha:1.000];
    UIColor *endColor = [UIColor colorWithRed:0.157 green:0.741 blue:0.561 alpha:1.000];

    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)startColor.CGColor, (id)endColor.CGColor];
    gradient.startPoint = CGPointMake(0, 1);
    gradient.endPoint = CGPointMake(1, 0);
    [self.view.layer insertSublayer:gradient atIndex:0];
}

#pragma mark - Actions

- (IBAction)logInButtonPressed:(id)sender {
    if (!self.loginTextField.text.length) {
        [self.loginTextField prv_shake];
        return;
    }
    if (!self.passwordTextField.text.length) {
        [self.passwordTextField prv_shake];
        return;
    }
    [self prv_addMBProgressHUD];
    NSDictionary *params = @{@"login":self.loginTextField.text,
                             @"password":self.passwordTextField.text};
    __weak typeof(self) weakSelf = self;
    [[PRVServerManager sharedManager] logInWithParams:params onSuccess:^(PRVUserServerModel *user, NSString *error) {
        [self prv_hideMBProgressHUD];
        if (error) {
            [self prv_showMessage:error withTitle:@"Ошибка"];
        } else {
            PRVMainViewController *vc = [[PRVMainViewController alloc] prv_initWithNib];
            vc.user = user;
            UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
            [weakSelf presentViewController:navVC animated:YES completion:nil];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self prv_showMessage:error.localizedDescription withTitle:@"Ошибка"];
        [self prv_hideMBProgressHUD];
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.loginTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}

@end
