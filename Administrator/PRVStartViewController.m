//
//  PRVStartViewController.m
//  Administrator
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVStartViewController.h"
#import "PRVServerManager.h"
#import "PRVAppManager.h"
#import "PRVMainViewController.h"
#import "PRVSignInViewController.h"

@implementation PRVStartViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
}

#pragma mark - Methods

- (void)loadData{
    __weak typeof(self) weakSelf = self;
    [self prv_addMBProgressHUD];
    [[PRVServerManager sharedManager] loadOptionsOnSuccess:^{
        [self prv_hideMBProgressHUD];
        if ([[PRVAppManager sharedInstance] getUser]) {
            PRVMainViewController *vc = [[PRVMainViewController alloc] prv_initWithNib];
            vc.user = [[PRVAppManager sharedInstance] getUser];
            UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
            [self presentViewController:navVC animated:YES completion:nil];
        } else {
            PRVSignInViewController *vc = [[PRVSignInViewController alloc] prv_initWithNib];
            [self presentViewController:vc animated:NO completion:nil];
        }
    } onFailure:^(NSError *error) {
        [self prv_hideMBProgressHUD];
        [self prv_showMessage:error.localizedDescription withRepeatHandler:^(UIAlertAction *action) {
            [weakSelf loadData];
        }];
    }];
}

@end
