//
//  PRVTicketTableViewCell.h
//  FitnessApp
//
//  Created by Ruslan on 8/18/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PRVTicketTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *clubLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *coachImageView;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@end
