//
//  UITableViewCell+PRVNib.h
//  FitnessApp
//
//  Created by Ruslan on 8/25/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (PRVNib)

+ (UINib *)prv_nib;

@end
