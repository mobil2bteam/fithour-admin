//
//  PRVCacheManager.m
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVCacheManager.h"
#import <MagicalRecord/MagicalRecord.h>
#import "PRVCache+CoreDataProperties.h"
#import "NSString+PRVExtension.h"
#import "NSDictionary+PRVExtension.h"

@implementation PRVCacheManager

+ (PRVCache *)cacheForKey:(NSString *)key{
    return [self cacheForKey:key andParameters:nil];
}

+ (PRVCache *)cacheForKey:(NSString *)key andParameters:(NSDictionary *)parameters{
    NSArray *allCache = [PRVCache MR_findAll];
    for (PRVCache *cache in allCache) {
        if ([cache.key isEqualToString:key]) {
            if (parameters) {
                if ([cache.params isEqualToString:[parameters stringValue]]) {
                    return cache;
                }
                continue;
            }
            return cache;
        }
    }
    return nil;
}

+ (void)updateCache{
    NSArray *allCache = [PRVCache MR_findAll];
    for (PRVCache *cache in allCache) {
        // если кэш устарел, то удаляем его
        NSDate *cacheDate = [cache.date dateByAddingTimeInterval:[cache.seconds doubleValue]];
        if ([cacheDate compare:[NSDate date]] == NSOrderedAscending) {
            [cache MR_deleteEntity];
        }
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        
    }];
}

+ (void)clearCache{
    NSArray *allCache = [PRVCache MR_findAll];
    for (PRVCache *cache in allCache) {
        [cache MR_deleteEntity];
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        
    }];
}

+ (void)setCache:(NSDictionary *)json forKey:(NSString *)key{
    [self setCache:json withParameters:nil forKey:key];
}

+ (void)setCache:(NSDictionary *)json withParameters:(NSDictionary *)parameters forKey:(NSString *)key{
    PRVCache *cashe = [PRVCache MR_createEntity];
    cashe.date = [NSDate date];
    cashe.key = key;
    cashe.json = [json stringValue];
    cashe.seconds = @([json[@"cache"] integerValue]);
    if (parameters) {
        cashe.params = [parameters stringValue];
    }
    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
    }];
}

@end
