//
//  PRVMainViewController.m
//  Administrator
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVMainViewController.h"
#import "PRVUserServerModel.h"
#import "PRVClubPreviewView.h"
#import "PRVTicketActionAlertViewController.h"
#import <QRCodeReaderViewController.h>
#import "PRVTicketDetailViewController.h"
#import "PRVSignInViewController.h"
#import "UIScrollView+EmptyDataSet.h"
#import "PRVTicketTableViewCell+Configuration.h"
#import "PRVServerManager.h"
#import "PRVLogoutViewController.h"

@interface PRVMainViewController () <QRCodeReaderDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet PRVClubPreviewView *clubInfoView;
@property (nonatomic, strong) QRCodeReaderViewController *qrCodeReaderViewController;
@property (weak, nonatomic) IBOutlet UITableView *ticketsTableView;
@property (nonatomic, strong) NSArray <PRVTicketSeverModel *> *tickets;
@property (nonatomic, strong) NSArray <PRVTicketSeverModel *> *allTickets;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation PRVMainViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Главная";
    // Register cell
    NSString *cellIdentifier = NSStringFromClass([PRVTicketTableViewCell class]);
    [self.ticketsTableView registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellReuseIdentifier:cellIdentifier];
    self.ticketsTableView.emptyDataSetDelegate = self;
    self.ticketsTableView.emptyDataSetSource = self;
    [self.clubInfoView configureForClub:self.user.info.club];
    // add pull to refresh to update tickets status
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.ticketsTableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTicketList) forControlEvents:UIControlEventValueChanged];
    // add logout button to navigation bar
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc]initWithTitle:@"Выйти" style:UIBarButtonItemStylePlain target:self action:@selector(logoutBarButtonPressed:)];
    rightButton.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = rightButton;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

#pragma mark - Getters

- (QRCodeReaderViewController *)qrCodeReaderViewController{
    if (_qrCodeReaderViewController) {
        return _qrCodeReaderViewController;
    }
    QRCodeReader *reader = [QRCodeReader readerWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]];
    
    // Instantiate the view controller
    QRCodeReaderViewController *qrCodeReaderViewController = [QRCodeReaderViewController readerWithCancelButtonTitle:@"Отменить" codeReader:reader startScanningAtLoad:YES showSwitchCameraButton:YES showTorchButton:YES];
    
    qrCodeReaderViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    qrCodeReaderViewController.delegate = self;
    _qrCodeReaderViewController = qrCodeReaderViewController;
    return _qrCodeReaderViewController;
}

#pragma mark - Actions

- (IBAction)logoutBarButtonPressed:(id)sender {
    __weak typeof(self) weakSelf = self;
    PRVLogoutViewController *vc = [[PRVLogoutViewController alloc] prv_initWithNib];
    vc.logoutBlock = ^{
        typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf logout];
    };
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)qrCodeButtonPressed:(id)sender {
    if ([QRCodeReader isAvailable]){
        [self presentViewController:self.qrCodeReaderViewController animated:YES completion:NULL];
    } else {
        [self prv_showMessage:@"Ваше устройство не поддерживает сканирование QR-кода" withTitle:nil];
    }
}

- (IBAction)textFieldDidChange:(id)sender {
    [self.ticketsTableView reloadData];
}

#pragma mark - Methods

- (void)logout{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"login"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    PRVSignInViewController *vc = [[PRVSignInViewController alloc] prv_initWithNib];
    [[UIApplication sharedApplication] delegate].window.rootViewController = vc;
}

- (void)refreshTicketList{
    __weak typeof(self) weakSelf = self;
    [[PRVServerManager sharedManager] getTicketListOnSuccess:^(NSArray<PRVTicketSeverModel *> *tickets, NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf.refreshControl endRefreshing];
        if (error) {
            [strongSelf prv_showMessage:error withTitle:nil];
        } else {
            [strongSelf.ticketsTableView reloadData];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf.refreshControl endRefreshing];
        [strongSelf prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

- (void)showTicketDetailViewControllerWithTicketCode:(NSString *)code{
    PRVTicketSeverModel *ticket = [self.user ticketForCode:code];
    if (ticket) {
        PRVTicketDetailViewController *vc = [[PRVTicketDetailViewController alloc] prv_initWithNib];
        vc.ticket = [self.user ticketForCode:code];
        [self.navigationController pushViewController:vc animated:YES];
    } else {
        [self prv_showMessage:@"Данный билет отстутствует в списке билетов" withTitle:nil];
    }
}

#pragma mark - QRCodeReaderViewControllerDelegate

- (void)readerDidCancel:(QRCodeReaderViewController *)reader{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)reader:(QRCodeReaderViewController *)reader didScanResult:(NSString *)result{
    __weak typeof(self) weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        typeof(self) strongSelf = weakSelf;
        [strongSelf showTicketDetailViewControllerWithTicketCode:result];
    }];
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Нет билетов";
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - Getters

- (NSArray <PRVTicketSeverModel *> *)allTickets{
    return self.user.info.tickets;
}

- (NSArray <PRVTicketSeverModel *> *)tickets{
    if (self.textField.text.length) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user.info.name CONTAINS[cd] %@ || code CONTAINS[cd] %@", self.textField.text, self.textField.text];
        return [self.allTickets filteredArrayUsingPredicate:predicate];
    } else {
        return self.allTickets;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return self.tickets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PRVTicketTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PRVTicketTableViewCell class]) forIndexPath:indexPath];
    [cell configureCellForTicket:self.tickets[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PRVTicketDetailViewController *vc = [[PRVTicketDetailViewController alloc] prv_initWithNib];
    vc.ticket = self.tickets[indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
