//
//  PRVRestorePasswordAlertVC.m
//  FitnessApp
//
//  Created by Ruslan on 9/15/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVRestorePasswordAlertVC.h"
#import "UIViewController+PRVExtensions.h"
#import "UIView+PRVExtensions.h"
#import "PRVServerManager.h"

@interface PRVRestorePasswordAlertVC ()

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@end

@implementation PRVRestorePasswordAlertVC

#pragma mark - Actions

- (IBAction)cancelButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)restorePasswordButtonPressed:(id)sender {
    [self.view endEditing:YES];
    if (!self.emailTextField.text.length) {
        [self.emailTextField prv_shake];
        return;
    }
    [self prv_addMBProgressHUD];
    __weak typeof(self) weakSelf = self;
    [[PRVServerManager sharedManager] restorePasswordWithEmail:self.emailTextField.text onSuccess:^(PRVUserServerModel *user, NSString *error) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        if (error) {
            [strongSelf prv_showMessage:error withTitle:nil];
        } else {
            __weak typeof(self) weakSelf = strongSelf;
            [strongSelf prv_showMessage:@"Пароль отправлен на почту" withOkHandler:^(UIAlertAction *action) {
                typeof(self) strongSelf = weakSelf;
                [strongSelf dismissViewControllerAnimated:YES completion:nil];
            }];
        }
    } onFailure:^(NSError *error, NSInteger statusCode) {
        typeof(self) strongSelf = weakSelf;
        [strongSelf prv_hideMBProgressHUD];
        [strongSelf prv_showMessage:error.localizedDescription withTitle:nil];
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

@end
