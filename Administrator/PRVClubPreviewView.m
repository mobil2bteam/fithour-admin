//
//  PRVClubPreviewView.m
//  FitnessApp
//
//  Created by Ruslan on 8/11/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVClubPreviewView.h"
#import "PRVClubsServerModel.h"
#import "UIImageView+PRVExtension.h"

@interface PRVClubPreviewView()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *reviewView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@end


@implementation PRVClubPreviewView

#pragma mark - Lifecycle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        [[NSBundle mainBundle]loadNibNamed:className owner:self options:nil];
        [self addSubview:_contentView];
        self.contentView.frame = self.bounds;
        [self customize];
    }
    return self;
}

- (void)customize{
    self.reviewView.backgroundColor = [UIColor secondPrimaryColor];
    self.distanceLabel.textColor = [UIColor primaryColor];
}

- (void)setNumberOfReviews:(NSInteger)number{
    if (number) {
        self.reviewsCountLabel.text = [NSString stringWithFormat:@" Оценили %ld чел.", (long)number];
    } else {
        self.reviewsCountLabel.text = @"Нет отзывов";
    }
}

- (void)configureForClub:(PRVClubServerModel *)club{
    self.bottomView.hidden = YES;
    [self setNumberOfReviews:club.count_reviews];
    self.clubNameLabel.text = club.name;
    self.clubAddressLabel.text = club.address;
    self.ratingLabel.text = [NSString stringWithFormat:@"%.1f", club.rating];
    [self.clubLogoImageView prv_setImageWithURL:club.logo];
    CGFloat distanseInKM = club.distance / 1000.f;
    self.distanceLabel.text = [NSString stringWithFormat:@"%.1f км", distanseInKM];
    if (club.work.free_day) {
        self.workingTimeLabel.text = @"Выходной";
    } else {
        self.workingTimeLabel.text = [NSString stringWithFormat:@"Работает до: %@", club.work.hours_finish];
    }
}

- (IBAction)removeButtonPressed:(id)sender {
    if (self.deleteBlock) {
        self.deleteBlock();
    }
}

@end
