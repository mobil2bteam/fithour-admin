//
//  AppDelegate.m
//  Administrator
//
//  Created by Ruslan on 9/20/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "AppDelegate.h"
#import "PRVStartViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "PRVCacheManager.h"
#import <MagicalRecord/MagicalRecord.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTintColor:[UIColor primaryColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    // add Fabric to testing app
    [Fabric with:@[[Crashlytics class]]];
    
    // add IQKeyboardManager to automatically scroll view when keyboard is showing
    [IQKeyboardManager sharedManager].enable = YES;
    
    // set up MagicalRecord
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    [PRVCacheManager clearCache];
    
    // init window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    // set up start view controller
    PRVStartViewController *startVC = [[PRVStartViewController alloc]prv_initWithNib];
    self.window.rootViewController = startVC;
    [self.window makeKeyAndVisible];
    return YES;
}

@end
