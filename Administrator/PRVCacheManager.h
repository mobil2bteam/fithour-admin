//
//  PRVCacheManager.h
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PRVCache;

@interface PRVCacheManager : NSObject

/*!
 * @discussion Get cache data
 * @param key - key for cache
 * @return Cache or nil if cache was not found
 */
+ (PRVCache *)cacheForKey:(NSString *)key andParameters:(NSDictionary *)parameters;

+ (PRVCache *)cacheForKey:(NSString *)key;

/*!
 * @discussion Update cache, if some cache has expired than it'll be removed
 */
+ (void)updateCache;

/*!
 * @discussion Add JSON to cache
 */
+ (void)setCache:(NSDictionary *)json withParameters:(NSDictionary *)parameters forKey:(NSString *)key;

/*!
 * @discussion Add JSON to cache
 */
+ (void)setCache:(NSDictionary *)json forKey:(NSString *)key;

/*!
 * @discussion Clear all cache
 */
+ (void)clearCache;

@end
