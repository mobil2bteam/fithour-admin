//
//  PRVServerManager.m
//  FitnessApp
//
//  Created by Ruslan on 7/10/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//

#import "PRVServerManager.h"
#import "PRVCacheManager.h"
#import "AFNetworking.h"
#import "Promise.h"
#import "PRVCache+CoreDataProperties.h"
#import "PRVOptionsServerModel.h"
#import "PRVClubsServerModel.h"
#import "AFHTTPSessionOperation.h"
#import "PRVAppManager.h"
#import "PRVUserServerModel.h"

@interface PRVServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager *requestOperationManager;

@end


@implementation PRVServerManager

+ (instancetype)sharedManager{
    static PRVServerManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PRVServerManager alloc] init];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@""]];
        manager.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
        [manager.requestOperationManager.requestSerializer setTimeoutInterval:180];
        manager.requestOperationManager.responseSerializer= [AFJSONResponseSerializer serializer];
    });
    return manager;
}

#pragma mark - Methods

- (void)loadOptionsOnSuccess:(void(^)()) success
                  onFailure:(void(^)(NSError *error)) failure {
    [self loadOptions].then(^(PRVOptionsServerModel *options) {
        [[PRVAppManager sharedInstance] setOptions:options];
        return [self checkUser];
    }).then(^(PRVUserServerModel *user){
        success();
    }).catch(^(NSError *error) {
        failure(error);
    });
}

- (PMKPromise *)loadOptions{
    return [PMKPromise new:^(PMKPromiseFulfiller fulfill, PMKPromiseRejecter reject){
        PRVCache *cache = [PRVCacheManager cacheForKey:@"options"];
        if (cache) {
            fulfill([[PRVOptionsServerModel alloc] initWithCache:cache]);
            return;
        }
        [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:@"GET" URLString:@"http://cabinet.fithour.ru/apiManager/" parameters:nil uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            if (responseObject[@"options"]) {
                PRVOptionsServerModel *options = [EKMapper objectFromExternalRepresentation:responseObject withMapping:[PRVOptionsServerModel objectMapping]];
                if (responseObject[@"cache"]) {
                    [PRVCacheManager setCache:responseObject forKey:@"options"];
                }
                fulfill(options);
            } else {
                reject([[NSError alloc]init]);
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            reject(error);
        }] start];
    }];
}

- (PMKPromise *)checkUser{
    return [PMKPromise new:^(PMKPromiseFulfiller fulfill, PMKPromiseRejecter reject){
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSString *login = [userDefaults stringForKey:@"login"];
        NSString *password = [userDefaults stringForKey:@"password"];
        
        if (login && password) {
            [self logInWithParams:@{@"login":login, @"password":password} onSuccess:^(PRVUserServerModel *user, NSString *error) {
                fulfill(user);
            } onFailure:^(NSError *error, NSInteger statusCode) {
                reject(error);
            }];
        } else {
            fulfill(nil);
        }
    }];
}


- (void)logInWithParams:(NSDictionary *)params
                    onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
                    onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVUserServerModel *user;
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_login"];
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"response user - %@", responseObject);
        
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"user"]) {
            user = [EKMapper objectFromExternalRepresentation:responseObject[@"user"] withMapping:[PRVUserServerModel objectMapping]];
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:params[@"password"] forKey:@"password"];
            [userDefaults setObject:params[@"login"] forKey:@"login"];
            [userDefaults synchronize];
            [[PRVAppManager sharedInstance] setUser:user];
        }
        
        if (success) {
            success(user, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)restorePasswordWithEmail:(NSString *)email
                       onSuccess:(void(^)(PRVUserServerModel *user, NSString *error)) success
                       onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVUserServerModel *user;
    NSDictionary *params = @{@"email":email};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_user_reset_password"];
    if (!url) {
        success(nil, @"Ошибка");
        return;
    }
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"user"]) {
            user = [EKMapper objectFromExternalRepresentation:responseObject[@"user"] withMapping:[PRVTicketSeverModel objectMapping]];
        }
        if (success) {
            success(user, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)payTicketWithCode:(NSString *)code
                  onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
                  onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVTicketSeverModel *ticket;
    NSDictionary *params = @{@"code":code,
                             @"token":[[PRVAppManager sharedInstance] getUser].info.accessToken};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_ticket_pay"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"ticket"]) {
            ticket = [EKMapper objectFromExternalRepresentation:responseObject[@"ticket"] withMapping:[PRVTicketSeverModel objectMapping]];
            [[[PRVAppManager sharedInstance] getUser] updateTicket:ticket];
        }
        if (success) {
            success(ticket, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)getTicketListOnSuccess:(void(^)(NSArray <PRVTicketSeverModel *> *tickets, NSString *error)) success
                onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block NSMutableArray <PRVTicketSeverModel *> *tickets = [[NSMutableArray alloc] init];
    NSDictionary *params = @{@"token":[[PRVAppManager sharedInstance] getUser].info.accessToken};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_tickets_list"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:url.metod URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"tickets"]) {
            for (NSDictionary *dic in responseObject[@"tickets"]) {
                PRVTicketSeverModel *ticket = [EKMapper objectFromExternalRepresentation:dic withMapping:[PRVTicketSeverModel objectMapping]];
                [tickets addObject:ticket];
            }
            [[PRVAppManager sharedInstance] getUser].info.tickets = [tickets copy];
        }
        if (success) {
            success([tickets copy], error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

- (void)updateTicketWithCode:(NSString *)code
                       enter:(BOOL)enter
                   onSuccess:(void(^)(PRVTicketSeverModel *ticket, NSString *error)) success
                   onFailure:(void(^)(NSError *error, NSInteger statusCode)) failure{
    __block NSString *error = nil;
    __block PRVTicketSeverModel *ticket;
    NSDictionary *params = @{@"code":code,
                             @"action":enter ? @"in" : @"out",
                             @"token":[[PRVAppManager sharedInstance] getUser].info.accessToken};
    
    PRVUrlServerModel *url = [[[PRVAppManager sharedInstance] getOptions] urlForName:@"url_ticket_update"];
    
    [[AFHTTPSessionOperation operationWithManager:self.requestOperationManager HTTPMethod:@"POST" URLString:url.url parameters:params uploadProgress:nil downloadProgress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        if (responseObject[@"error"]) {
            error = responseObject[@"error"][@"text"];
        } else if (responseObject[@"ticket"]) {
            ticket = [EKMapper objectFromExternalRepresentation:responseObject[@"ticket"] withMapping:[PRVTicketSeverModel objectMapping]];
            [[[PRVAppManager sharedInstance] getUser] updateTicket:ticket];
        }
        if (success) {
            success(ticket, error);
        }
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        if (failure)
        {
            failure(error,error.code);
        }
    }] start];
}

@end
