//
//  PRVClubInfoView.h
//  FitnessApp
//
//  Created by Ruslan on 7/27/17.
//  Copyright © 2017 Ruslan Palapa. All rights reserved.
//
@class PRVClubServerModel;

#import <UIKit/UIKit.h>

@interface PRVClubInfoView : UIView

- (void)setClub:(PRVClubServerModel *)club;

- (void)setAddressLabelFontSize:(NSInteger)size;

@end
